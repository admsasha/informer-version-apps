#ifndef WIDGETWAITING_H
#define WIDGETWAITING_H

#include <QWidget>

namespace Ui {
class WidgetWaiting;
}

class WidgetWaiting : public QWidget{
    Q_OBJECT

    public:
        explicit WidgetWaiting(QWidget *parent = nullptr);
        ~WidgetWaiting();

        void setTitleText(QString text);
        void setProgressBarValue(int current_value,int max_value);

    private:
        Ui::WidgetWaiting *ui;
};

#endif // WIDGETWAITING_H
