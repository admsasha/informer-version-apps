#include "WidgetWaiting.h"
#include "ui_WidgetWaiting.h"

WidgetWaiting::WidgetWaiting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetWaiting)
{
    ui->setupUi(this);
}

WidgetWaiting::~WidgetWaiting()
{
    delete ui;
}

void WidgetWaiting::setTitleText(QString text){
    ui->label->setText(text);
}

void WidgetWaiting::setProgressBarValue(int current_value, int max_value){
    ui->progressBar->setMaximum(max_value);
    ui->progressBar->setValue(current_value);
}
