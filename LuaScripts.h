#ifndef LUASCRIPTS_H
#define LUASCRIPTS_H

#include <QStringList>

class LuaScripts {
    public:
        LuaScripts();

        static QStringList getScriptList();
        static QString getPathAppTag(const QString &tag);

};

#endif // LUASCRIPTS_H
