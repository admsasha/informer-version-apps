#include "LuaScripts.h"

#include <QDir>
#include <QStandardPaths>
#include "config_informer.h"

LuaScripts::LuaScripts(){

}

QStringList LuaScripts::getScriptList(){
    QStringList listLuaScripts;

    QDir dirUserLua(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/apps");
    dirUserLua.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QFileInfoList listUserLua = dirUserLua.entryInfoList();
    for (QFileInfo &fileinfo : listUserLua) {
        if (fileinfo.fileName().right(4)!=".lua") continue;
        listLuaScripts.push_back(fileinfo.fileName());
    }

    QDir dir(QString(GLOBAL_PATH_USERDATA)+"/apps");
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QFileInfoList list = dir.entryInfoList();
    for (QFileInfo &fileinfo : list) {
        if (fileinfo.fileName().right(4)!=".lua") continue;
        if (listLuaScripts.contains(fileinfo.fileName())) continue;
        listLuaScripts.push_back(fileinfo.fileName());
    }

    return listLuaScripts;
}


QString LuaScripts::getPathAppTag(const QString &tag){
    QFile fileUserHome(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/apps/"+tag+".lua");
    if (fileUserHome.exists()) return fileUserHome.fileName();

    QFile fileSystem(QString(GLOBAL_PATH_USERDATA)+"/apps/"+tag+".lua");
    if (fileSystem.exists()) return fileSystem.fileName();

    return "";
}
