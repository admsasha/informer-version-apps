#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QSettings>
#include <QTimer>
#include <QMenu>
#include <QAction>

#include "LuaContext.hpp"
#include "CTray.h"
#include "WidgetWaiting.h"
#include "ThreadAutoUpdate.h"
#include "ManagerApps.h"

namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private:
        Ui::MainWindow *ui;

        void initToolButton();
        void initMenuContextTableUrl();
        void initSupportApps();
        void reloadSupportApps();

        QLabel *lblSite;
        QLabel *lblInfo;
        QLabel *lblInfoSupportApps;

        CTray *tray;
        QSettings *confSettings;


        QMenu *menuContextTableUrl;
        QAction *actionContextTableUrlCopyUrl;

        WidgetWaiting * wWaiting;
        QLabel *fake_background;

        QTimer *timerUpdate;

        ThreadAutoUpdate *thAutoUpdate;

        QMap<QString,APP_STRUCT> listSupportApps;

        APP_STRUCT getCurrentVersion(QString tag);

        void updateVersionFromLocalRepo();
        QMap<QString,QString> appLocalRepoVersionsList;

        void restoreTables();
        void addAppToTable(APP_STRUCT struct_app);
        bool addAppToTable(const QString &tag);
        void checkLatestVersion(QStringList tags=QStringList());

        void waitingShow(QString title);
        void waitingSetTitle(QString title);
        void waitingSetProgressBar(int current,int max);
        void waitingHide();

    private slots:
        void showMe();
        void myExit();

        void check();
        void searchApps();
        void addApps();
        void deleteApps();
        void showSetup();
        void showAbout();

        void thAutoUpdate_update(int countUpdateFiles);
        void timerUpdate_timeout();

        void popupCustomMenu(const QPoint &pos);
        void contextTableUrlCopyUrl();
};

#endif // MAINWINDOW_H
