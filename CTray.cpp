#include "CTray.h"

CTray::CTray(QWidget *parent) : QWidget(parent){
    openAction = new QAction(tr("Open"), this);
    quitAction = new QAction(tr("Exit"), this);

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(openAction);
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/informer.png"));
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->show();

    connect(openAction, SIGNAL(triggered()), this, SIGNAL(trayOpenAction()));
    connect(quitAction, SIGNAL(triggered()), this, SIGNAL(trayClickQuit()));

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
}

CTray::~CTray(){
    delete trayIcon;
}


void CTray::showMessage(QString title,QString msg,int msecs){
    trayIcon->showMessage(title,msg,QSystemTrayIcon::Information,msecs);
}

void CTray::iconActivated(QSystemTrayIcon::ActivationReason reason){
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        emit trayDoubleClick();
        break;
    case QSystemTrayIcon::MiddleClick:
        emit trayMiddleClick();
        break;
    default:
        ;
    }
}
