#include "MainWindow.h"
#include "ui_MainWindow.h"


#include <QString>
#include <QDebug>
#include <QProcess>
#include <QProcessEnvironment>
#include <QFile>
#include <QLabel>
#include <QByteArray>
#include <QDir>
#include <QFileInfoList>
#include <QMessageBox>
#include <QDir>
#include <QSettings>
#include <QClipboard>
#include <QStandardPaths>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>

#include "config_informer.h"
#include "LuaScripts.h"
#include "DownloadManager.h"
#include "LuaContext.hpp"
#include "FormAbout.h"
#include "FormAddApp.h"
#include "FormSetup.h"
#include "InfoOs.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Informer of new versions of programs v%1").arg(INFORMER_VERSION));
    this->setWindowIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/informer.png"));

    initToolButton();
    initMenuContextTableUrl();

    QDir dirConfig(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    if (dirConfig.exists()==false) dirConfig.mkpath(dirConfig.path());

    QDir dirUserLua(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/apps");
    if (dirUserLua.exists()==false) dirUserLua.mkpath(dirUserLua.path());


    confSettings = new QSettings(dirConfig.absoluteFilePath("settings.ini"), QSettings::IniFormat);
    confSettings->setPath(QSettings::IniFormat, QSettings::UserScope, QDir::currentPath());

    fake_background = new QLabel(this);
    fake_background->setGeometry(0,0,this->width(),this->height());
    fake_background->hide();

    wWaiting = new WidgetWaiting(this);
    wWaiting->move((this->width()-wWaiting->width())/2,(this->height()-wWaiting->height())/2);
    wWaiting->hide();

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->hideColumn(0);
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2,QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(3,QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(4,QHeaderView::Stretch);

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(const QPoint & )),SLOT(popupCustomMenu(const QPoint &)));

    lblInfo = new QLabel(this);
    ui->statusbar->addWidget(lblInfo,0);

    lblInfoSupportApps =  new QLabel(this);
    ui->statusbar->addWidget(lblInfoSupportApps,1);

    lblSite = new QLabel(this);
    lblSite->setOpenExternalLinks(true);
    ui->statusbar->addWidget(lblSite,0);

    tray = new CTray(this);
    connect(tray,SIGNAL(trayClickQuit()),this,SLOT(myExit()));
    connect(tray,SIGNAL(trayOpenAction()),this,SLOT(showMe()));
    connect(tray,SIGNAL(trayDoubleClick()),this,SLOT(showMe()));
    tray->show();

    timerUpdate = new QTimer(this);
    connect(timerUpdate,SIGNAL(timeout()),this,SLOT(timerUpdate_timeout()));
    timerUpdate->start(3*1000);

    thAutoUpdate = new ThreadAutoUpdate();
    connect(thAutoUpdate,SIGNAL(update(int)),this,SLOT(thAutoUpdate_update(int)));
    thAutoUpdate->start();

    lblInfo->setText(tr("Controlled apps: 0")+"  ");
    lblInfoSupportApps->setText(tr("Count of supported programs: 0"));
    lblSite->setText("<a href=\"https://appversions.dansoft.ru\">https://appversions.dansoft.ru</a>");

    show();

    // download support apps
    initSupportApps();

    reloadSupportApps();

    // Start!
    thAutoUpdate_update(0);
    restoreTables();
}

MainWindow::~MainWindow(){
    QStringList tags;
    for (int r=0;r<ui->tableWidget->rowCount();r++){
        tags.push_back(ui->tableWidget->item(r,0)->text());
    }
    confSettings->setValue("setup/tags",tags.join(","));

    delete lblInfo;
    delete lblSite;
    delete tray;
    delete confSettings;
    delete ui;
}


void MainWindow::initToolButton(){
    ui->toolButton->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/add.png"));
    ui->toolButton_3->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/check.png"));
    ui->toolButton_7->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/search.png"));
    ui->toolButton_2->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/delete.png"));
    ui->toolButton_4->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/setup.png"));
    ui->toolButton_5->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/about.png"));
    ui->toolButton_6->setIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/exit.png"));

    connect(ui->toolButton_3,SIGNAL(clicked(bool)),this,SLOT(check()));
    connect(ui->toolButton_7,SIGNAL(clicked(bool)),this,SLOT(searchApps()));
    connect(ui->toolButton,SIGNAL(clicked(bool)),this,SLOT(addApps()));
    connect(ui->toolButton_2,SIGNAL(clicked(bool)),this,SLOT(deleteApps()));
    connect(ui->toolButton_4,SIGNAL(clicked(bool)),this,SLOT(showSetup()));
    connect(ui->toolButton_5,SIGNAL(clicked(bool)),this,SLOT(showAbout()));
    connect(ui->toolButton_6,SIGNAL(clicked(bool)),this,SLOT(myExit()));

}

void MainWindow::initMenuContextTableUrl(){
    menuContextTableUrl = new QMenu(this);
    actionContextTableUrlCopyUrl = new QAction(tr("Copy url"), this);

    menuContextTableUrl->addAction(actionContextTableUrlCopyUrl);

    connect(actionContextTableUrlCopyUrl, SIGNAL(triggered()), this, SLOT(contextTableUrlCopyUrl()));

}

void MainWindow::initSupportApps(){
    QString currentListAppsMd5="";
    DownloadManager mng;

    QFile fileListAppsMd5(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/listApps.md5");
    if (fileListAppsMd5.open(QIODevice::ReadOnly | QIODevice::Text)){
        currentListAppsMd5 = fileListAppsMd5.readAll();
        fileListAppsMd5.close();
    }

    waitingShow(tr("Check update list supported programs"));
    QString listAppsMd5 =  mng.execute("https://appversions.dansoft.ru/api/listApps.md5?version="+QString(INFORMER_VERSION),false,"get");
    waitingHide();
    if (mng.getStatusCode()!=200) return;

    if (currentListAppsMd5!=listAppsMd5){
        waitingShow(tr("Download list supported programs"));
        QString listAppsJson=  mng.execute("https://appversions.dansoft.ru/api/listApps.json?version="+QString(INFORMER_VERSION),false,"get");
        waitingHide();
        if (mng.getStatusCode()!=200) return;

        QFile fileListAppsJson(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/listApps.json");
        if (fileListAppsJson.open(QIODevice::WriteOnly | QIODevice::Text)){
            fileListAppsJson.write(listAppsJson.toUtf8());
            fileListAppsJson.close();
        }


        if (fileListAppsMd5.open(QIODevice::WriteOnly | QIODevice::Text)){
            fileListAppsMd5.write(listAppsMd5.toUtf8());
            fileListAppsMd5.close();
        }
    }
}

void MainWindow::reloadSupportApps(){
    listSupportApps.clear();

    QFile fileListAppsJson(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/listApps.json");
    if (!fileListAppsJson.open(QIODevice::ReadOnly | QIODevice::Text)) return;

    QByteArray json_data = fileListAppsJson.readAll();
    QJsonDocument document = QJsonDocument::fromJson(json_data);

    if (!document.isEmpty()){
        QJsonArray root = document.array();
        for (QJsonValue json: root){
            APP_STRUCT app;

            app.tag = json.toObject().value("tag").toString();
            app.name = json.toObject().value("name").toString();
            app.summary = QByteArray::fromBase64(json.toObject().value("summary").toString().toUtf8());
            app.url = json.toObject().value("url").toString();
            app.license = json.toObject().value("license").toString();

            listSupportApps[app.tag]=app;
        }
    }
    fileListAppsJson.close();

    lblInfoSupportApps->setText(tr("Count of supported programs: %1").arg(listSupportApps.size())+"  ");

}

void MainWindow::updateVersionFromLocalRepo(){
    INFORMATION_OS infoOS = InfoOs::getInfoOs();

    appLocalRepoVersionsList.clear();

    if (infoOS.typePkg=="rpm"){
        QProcess proc;
        proc.start("rpm -qa");
        if (!proc.waitForFinished()){
            return;
        }

        QRegExp re("(.*)(-)([\\d\\.]+)(-)(.*)");
        re.setMinimal(true);

        QString output = proc.readAll();
        QStringList lines = output.split("\n");
        for (QString line:lines){
            if (re.indexIn(line)!=-1){
                appLocalRepoVersionsList[re.cap(1).toLower()]=re.cap(3);
            }
        }

    }

    if (infoOS.typePkg=="dpkg"){
        QProcess proc;
        proc.start("dpkg-query -l");
        if (!proc.waitForFinished()){
            return;
        }

        QString output = proc.readAll();
        QStringList lines = output.split("\n");
        for (QString line:lines){
            QStringList p = line.split(QRegExp("[\\s]+"));
            if (p.size()>3){
                QString raw_version = p.at(2);
                QStringList raw_version_list = raw_version.split(":");
                if (raw_version_list.size()>1){
                    raw_version=raw_version_list.at(1);
                }

                QRegExp re("([\\d\\.]+)");
                if (re.indexIn(raw_version)!=-1){
                    QString version = re.cap(1);
                    if (version.right(1)==".") version=version.mid(0,version.length()-1);
                    appLocalRepoVersionsList[p.at(1).toLower()]=version;
                }
            }
        }
    }
}

APP_STRUCT MainWindow::getCurrentVersion(QString tag){
    APP_STRUCT result;

    if (listSupportApps.contains(tag)==0){
        return APP_STRUCT();
    }


    result.tag=tag;
    result.name=listSupportApps[tag].name;
    result.url=listSupportApps[tag].url;

    if (appLocalRepoVersionsList.contains(tag)!=0){
        result.current_version=appLocalRepoVersionsList[tag];
        return result;
    }


    // from lua scripts

    QString filename = LuaScripts::getPathAppTag(tag);
    if (filename.isEmpty()) return result;


    QString additional_paths = confSettings->value("setup/additional_paths","").toString();

    try {
        LuaContext lua;

        lua.writeFunction("exec_re", [&additional_paths](std::string command,std::string regexp,int position) {
            QProcess proc;

            QProcessEnvironment systemEnvironment = QProcessEnvironment::systemEnvironment();
            systemEnvironment.insert("LANG","C");
            systemEnvironment.insert("LC_ALL","C");
            systemEnvironment.insert("PATH",systemEnvironment.value("PATH")+":/usr/sbin:"+additional_paths);

            proc.setProcessEnvironment(systemEnvironment);
            proc.setReadChannelMode(QProcess::MergedChannels);

            QByteArray OldPath = qgetenv("PATH");
            qputenv("PATH", qgetenv("PATH") +QString(":/usr/sbin:"+additional_paths).toUtf8());

            proc.start(QString::fromStdString(command));
            if (!proc.waitForFinished()){
                qputenv("PATH",OldPath);
                return std::string("");
            }
            qputenv("PATH",OldPath);

            QByteArray data = proc.readAll();

            if (regexp=="") return data.toStdString();

            QRegExp re(QString::fromStdString(regexp));
            re.setMinimal(true);
            if (re.indexIn(data)!=-1){
                QString version = re.cap(position);
                version.replace("\n","");
                version.replace("\r","");
                return version.toStdString();
            }


            return std::string("");
        });


        QFile file(filename);
        if (!file.open(QIODevice::ReadOnly)) return result;
        QByteArray data=file.readAll();
        file.close();

        lua.executeCode(data);

        const auto function = lua.readVariable<std::function<std::string (void)>>("current_version");
        QString app_version = QString::fromStdString(function());
        result.current_version=app_version;

    }
    catch(const LuaContext::ExecutionErrorException& e) {
        qDebug() << e.what();
        try {
            std::rethrow_if_nested(e);
        } catch(const std::runtime_error& error) {
            qDebug() << error.what();
        }
    }
    catch(const LuaContext::WrongTypeException& e) {
        qDebug() << e.what();
    }
    catch(const LuaContext::SyntaxErrorException& e) {
        qDebug() << e.what();
    }
    catch(const std::runtime_error& e) {
        qDebug() << e.what();
    }
    catch(...){
        qDebug() << tr("unknow error");
    }


    return result;
}

void MainWindow::restoreTables(){
    waitingShow(tr("Request current versions..."));
    updateVersionFromLocalRepo();
    QStringList tags = confSettings->value("setup/tags","").toString().split(",");

    waitingSetProgressBar(0,tags.size());
    int count=0;

    for (QString tag:tags){
        if (tag.isEmpty()) continue;
        addAppToTable(tag);
        waitingSetProgressBar(count,tags.size());
        count++;
    }

    waitingSetTitle(tr("Request the latest versions of the software..."));
    checkLatestVersion();
    waitingHide();
}

void MainWindow::addAppToTable(APP_STRUCT struct_app){
    int index = -1;

    QString tag=struct_app.tag;


    // if tag exist
    for (int r=0;r<ui->tableWidget->rowCount();r++){
        if (tag==ui->tableWidget->item(r,0)->text()){
            index=r;
            break;
        }
    }

    if (index==-1){
        ui->tableWidget->setRowCount(ui->tableWidget->rowCount()+1);
        index = ui->tableWidget->rowCount()-1;

        QTableWidgetItem *tmp = nullptr;
        for (int col=0;col<ui->tableWidget->columnCount();col++){
            tmp = new QTableWidgetItem("");
            ui->tableWidget->setItem(index, col, tmp);
        }

    }


    ui->tableWidget->item(index,0)->setText(tag);
    ui->tableWidget->item(index,1)->setText(struct_app.name);
    ui->tableWidget->item(index,2)->setText(struct_app.current_version);
    ui->tableWidget->item(index,3)->setText(struct_app.last_version);
    ui->tableWidget->item(index,4)->setText(struct_app.url);

    ui->tableWidget->sortItems(0);

}

bool MainWindow::addAppToTable(const QString &tag){
    APP_STRUCT app = getCurrentVersion(tag);
    if (app.name.isEmpty()) return false;

    addAppToTable(app);

    return true;
}

void MainWindow::check(){
    waitingShow(tr("Request current versions..."));
    updateVersionFromLocalRepo();

    for (int r=0;r<ui->tableWidget->rowCount();r++){
        QString tag = ui->tableWidget->item(r,0)->text();

        APP_STRUCT app = getCurrentVersion(tag);
        waitingSetProgressBar(r,ui->tableWidget->rowCount());

        if (app.current_version!=""){
            ui->tableWidget->item(r, 2)->setText(app.current_version);
        }

        for (int col=0;col<ui->tableWidget->columnCount();col++){
            ui->tableWidget->item(r,col)->setBackgroundColor(QColor(255,255,255));
        }
    }

    waitingSetTitle(tr("Request the latest versions of the software..."));

    checkLatestVersion();

    waitingHide();
}

void MainWindow::searchApps(){
    int countAllApps=0;

    if (ui->tableWidget->rowCount()>0){
        if (QMessageBox::question(this,tr("Search apps"),tr("The entire current list will be deleted. Continue ?"))!=QMessageBox::Yes){
            return;
        }
    }


    ui->tableWidget->setRowCount(0);

    waitingShow(tr("Search for installed programs"));
    updateVersionFromLocalRepo();


    waitingSetProgressBar(0,listSupportApps.size());

    QMapIterator<QString,APP_STRUCT>i(listSupportApps);
    while (i.hasNext()) {
        i.next();
        QString tag = i.key();

        countAllApps++;
        APP_STRUCT app_struct = getCurrentVersion(tag);

        waitingSetProgressBar(countAllApps,listSupportApps.size());

        if (app_struct.current_version.isEmpty()) continue;

        addAppToTable(app_struct);
    }

    waitingSetTitle(tr("Request the latest versions of the software..."));
    checkLatestVersion();

    waitingHide();
}

void MainWindow::checkLatestVersion(QStringList tags){

    if (tags.size()==0){
        for (int i=0;i<ui->tableWidget->rowCount();i++){
            tags.push_back(ui->tableWidget->item(i,0)->text());
        }
    }

    DownloadManager mng;
    QByteArray json_data =  mng.execute("https://appversions.dansoft.ru/api/getVersions",false,"POST","version="+QString(INFORMER_VERSION)+"&tags="+tags.join(","));

    QJsonDocument document = QJsonDocument::fromJson(json_data);
    if (document.isEmpty()){
        return;
    }

    QJsonArray root = document.array();
    for (QJsonValue json: root){
        QString tag = json.toObject().value("tag").toString();
        QString version = json.toObject().value("version").toString();


        for (int i=0;i<ui->tableWidget->rowCount();i++){
            if (ui->tableWidget->item(i,0)->text()==tag){
                QString app_name = ui->tableWidget->item(i,1)->text();
                QString currentLastVersion = ui->tableWidget->item(i,3)->text();

                ui->tableWidget->item(i,3)->setText(version);

                if (confSettings->value("setup/notify_new_version","1").toBool()==true){
                    if (currentLastVersion!=version and currentLastVersion!=""){
                        tray->showMessage(tr("Informer Version Apps"),tr("Version %1 update to %2").arg(app_name).arg(version));
                    }
                }

                if (ui->tableWidget->item(i,2)->text()==version){
                    for (int col=0;col<ui->tableWidget->columnCount();col++){
                        ui->tableWidget->item(i,col)->setBackgroundColor(Qt::green);
                    }
                }else if (ui->tableWidget->item(i,2)->text() != ""){
                    for (int col=0;col<ui->tableWidget->columnCount();col++){
                        ui->tableWidget->item(i,col)->setBackgroundColor(Qt::red);
                    }
                }else{
                    for (int col=0;col<ui->tableWidget->columnCount();col++){
                        ui->tableWidget->item(i,col)->setBackgroundColor(Qt::white);
                    }
                }
            }
        }
    }

    confSettings->setValue("temp/lastUpdate",QDateTime::currentDateTime().toTime_t());
}

void MainWindow::waitingShow(QString title){
    fake_background->show();
    wWaiting->setTitleText(title);
    wWaiting->setProgressBarValue(0,0);
    wWaiting->show();
    QApplication::processEvents();
}

void MainWindow::waitingSetTitle(QString title){
    wWaiting->setTitleText(title);
    QApplication::processEvents();
}

void MainWindow::waitingSetProgressBar(int current, int max){
    wWaiting->setProgressBarValue(current,max);
    QApplication::processEvents();
}

void MainWindow::waitingHide(){
    fake_background->hide();
    wWaiting->hide();

    lblInfo->setText(QString(tr("Controlled apps: %1")+"   ").arg(ui->tableWidget->rowCount()));

    QApplication::processEvents();
}

void MainWindow::showMe(){
    this->show();
    this->activateWindow();
}

void MainWindow::myExit(){
    QApplication::closeAllWindows();
    QApplication::exit(0);
}

void MainWindow::addApps(){
    FormAddApp form(this,listSupportApps);
    if (form.exec()){
        QString selectTag = form.getSelectTag();

        waitingShow(tr("Adding app"));
        updateVersionFromLocalRepo();

        if (!addAppToTable(selectTag)){
            QMessageBox::critical(this,tr("Informer Version Apps"),tr("Error adding %1. Perhaps the script %2.lua is broken").arg(selectTag).arg(selectTag));
            waitingHide();
            return;
        }

        for (int r=0;r<ui->tableWidget->rowCount();r++){
            QString tag = ui->tableWidget->item(r,0)->text();

            if (selectTag==tag){
                ui->tableWidget->setCurrentCell(r,1);
            }
        }

        waitingSetTitle(tr("Request the latest versions of the software..."));
        checkLatestVersion(QStringList() << selectTag);
        waitingHide();
    }
}

void MainWindow::deleteApps(){
    if (ui->tableWidget->currentRow()==-1) return;
    ui->tableWidget->removeRow(ui->tableWidget->currentRow());
}

void MainWindow::showSetup(){
    FormSetup form(this);
    form.exec();
}

void MainWindow::showAbout(){
    FormAbout frm(this);
    frm.exec();
}

void MainWindow::thAutoUpdate_update(int countUpdateFiles){

}

void MainWindow::timerUpdate_timeout(){
    if (wWaiting->isVisible()) return;

    QString checker_frequency = confSettings->value("setup/checker_frequency","day").toString();
    uint lastUpdate = confSettings->value("temp/lastUpdate",0).toUInt();

    if (checker_frequency.indexOf(QRegExp("^(day|week|month)$"))==-1) return;

    if (checker_frequency=="day")
        if (QDateTime::currentDateTime().toTime_t()-lastUpdate<86400) return;

    if (checker_frequency=="week")
        if (QDateTime::currentDateTime().toTime_t()-lastUpdate<7*86400) return;

    if (checker_frequency=="month")
        if (QDateTime::currentDateTime().toTime_t()-lastUpdate<30*86400) return;

    check();
}

void MainWindow::popupCustomMenu(const QPoint &pos){
    menuContextTableUrl->popup(ui->tableWidget->mapToGlobal(pos));
}

void MainWindow::contextTableUrlCopyUrl(){
    if (ui->tableWidget->currentRow()==-1) return;

    QClipboard* c = QApplication::clipboard();
    c->setText(ui->tableWidget->item(ui->tableWidget->currentRow(),4)->text());
}
