#ifndef FORMADDAPP_H
#define FORMADDAPP_H

#include <QDialog>

#include "ManagerApps.h"

namespace Ui {
class FormAddApp;
}

class FormAddApp : public QDialog
{
    Q_OBJECT

    public:
        explicit FormAddApp(QWidget *parent, QMap<QString, APP_STRUCT> &listSupportApps);
        ~FormAddApp();

        QString getSelectTag();

    private slots:
        void append();

    private:
        Ui::FormAddApp *ui;
};

#endif // FORMADDAPP_H
