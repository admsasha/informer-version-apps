#ifndef MANAGERAPPS_H
#define MANAGERAPPS_H

#include <QString>

struct APP_STRUCT {
    QString tag;
    QString name;
    QString url;
    QString summary;
    QString license;
    QString current_version;
    QString last_version;
};

class ManagerApps
{
public:
    ManagerApps();
};

#endif // MANAGERAPPS_H
