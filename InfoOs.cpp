#include "InfoOs.h"

#include <QFile>
#include <QRegExp>
#include <QDebug>

INFORMATION_OS InfoOs::getInfoOs(){
    INFORMATION_OS result;

    QFile file("/etc/os-release");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return result;

    while (!file.atEnd()) {
        QString line = file.readLine();

        if (line.left(3)=="ID="){
            result.id=line.mid(3);

            if (line.indexOf("rosa")!=-1) result.typePkg="rpm";
            if (line.indexOf("altlinux")!=-1) result.typePkg="rpm";
            if (line.indexOf("opensuse")!=-1) result.typePkg="rpm";
            
        }

        if (line.left(8)=="ID_LIKE="){
            if (line.indexOf("debian")!=-1){
                result.typePkg="dpkg";
            }
        }
    }



    return result;
}
