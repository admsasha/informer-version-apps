#ifndef CTRAY_H
#define CTRAY_H

#include <QWidget>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QAction>

class CTray: public QWidget {
    Q_OBJECT

    public:
        CTray(QWidget *parent=nullptr);
        ~CTray();
        void showMessage(QString title, QString msg, int msecs=3000);

    private:
        QSystemTrayIcon *trayIcon;
        QMenu *trayIconMenu;

        QAction *openAction;
        QAction *quitAction;


    signals:
        void trayDoubleClick();
        void trayMiddleClick();
        void trayClickQuit();
        void trayOpenAction();

    public slots:
        void iconActivated(QSystemTrayIcon::ActivationReason reason);
};

#endif // CTRAY_H
