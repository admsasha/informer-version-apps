#include "FormSetup.h"
#include "ui_FormSetup.h"

#include <QIcon>
#include <QDir>
#include <QSettings>
#include <QStandardPaths>

#include "config_informer.h"

FormSetup::FormSetup(QWidget *parent) : QDialog(parent), ui(new Ui::FormSetup) {
    ui->setupUi(this);
    this->setFixedSize(this->width(),this->height());
    this->setWindowTitle(tr("Setup"));
    this->setWindowIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/informer.png"));

    QDir dirConfig(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    confSettings = new QSettings(dirConfig.absoluteFilePath("settings.ini"), QSettings::IniFormat);
    confSettings->setPath(QSettings::IniFormat, QSettings::UserScope, QDir::currentPath());

    ui->comboBox->addItem(tr("Not check"),"none");
    ui->comboBox->addItem(tr("Once a day"),"day");
    ui->comboBox->addItem(tr("Once a week"),"week");
    ui->comboBox->addItem(tr("Once a month"),"month");


    QString checker_frequency = confSettings->value("setup/checker_frequency","day").toString();
    for (int i=0;i<ui->comboBox->count();i++){
        if (ui->comboBox->itemData(i).toString()==checker_frequency){
            ui->comboBox->setCurrentIndex(i);
            break;
        }
    }


    ui->plainTextEdit->setPlainText(confSettings->value("setup/additional_paths","").toString());
    ui->checkBox->setChecked(confSettings->value("setup/notify_new_version","1").toBool());
    ui->checkBox_2->setChecked(confSettings->value("setup/update_lua","1").toBool());

    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(apply()));
    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(close()));
}

FormSetup::~FormSetup(){
    delete confSettings;
    delete ui;
}

void FormSetup::apply(){
    confSettings->setValue("setup/checker_frequency",ui->comboBox->currentData().toString());
    confSettings->setValue("setup/additional_paths",ui->plainTextEdit->toPlainText());
    confSettings->setValue("setup/notify_new_version",ui->checkBox->isChecked());
    confSettings->setValue("setup/update_lua",ui->checkBox_2->isChecked());

    accept();
}
