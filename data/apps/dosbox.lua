APP_NAME = "dosbox"
APP_URL = "https://www.dosbox.com/"

function current_version()
   return exec_re("dosbox --version","(DOSBox version )(.*)(,)",2)
end

