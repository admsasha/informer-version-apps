APP_NAME = "qemu"
APP_URL = "https://www.qemu.org/"

function current_version()
   return exec_re("qemu-kvm --version","(QEMU emulator version )(.*)( )",2)
end

