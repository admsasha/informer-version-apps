APP_NAME = "CMake"
APP_URL = "https://cmake.org/"

function current_version()
   return exec_re("cmake --version","(cmake version )(.*)(\n)",2)
end

