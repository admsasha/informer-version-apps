APP_NAME = "dovecot"
APP_URL = "https://www.dovecot.org/"

function current_version()
   return exec_re("dovecot --version","(.*)( )",1)
end

