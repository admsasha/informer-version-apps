APP_NAME = "mc"
APP_URL = "https://midnight-commander.org/"

function current_version()
   return exec_re("mc --version","(GNU Midnight Commander )(.*)(\n)",2)
end

