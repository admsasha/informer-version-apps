APP_NAME = "wireshark"
APP_URL = "https://www.wireshark.org"

function current_version()
   return exec_re("wireshark --version","(Wireshark )(.*)( )",2)
end

