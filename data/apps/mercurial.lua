APP_NAME = "Mercurial"
APP_URL = "https://www.mercurial-scm.org/"

function current_version()
   return exec_re("hg --version","(SCM Mercurial )(.*)( )(.*)(\\))",4)
end

