APP_NAME = "emacs"
APP_URL = "http://www.gnu.org/software/emacs/"

function current_version()
   return exec_re("emacs --version","(GNU Emacs )(.*)(\n)",2)
end

