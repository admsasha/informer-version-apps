APP_NAME = "digikam"
APP_URL = "https://www.digikam.org"

function current_version()
   return exec_re("digikam --version","(digikam)(.*)(\n)",2)
end

