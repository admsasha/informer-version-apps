APP_NAME = "UPX"
APP_URL = "https://upx.github.io"

function current_version()
   return exec_re("upx --version","(upx )(.*)(\n)",2)
end

