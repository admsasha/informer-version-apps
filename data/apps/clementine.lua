APP_NAME = "Clementine"
APP_URL = "https://www.clementine-player.org"

function current_version()
   return exec_re("clementine --version","(Clementine )(.*)(\n)",2)
end

