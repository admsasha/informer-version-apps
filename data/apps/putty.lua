APP_NAME = "putty"
APP_URL = "http://www.chiark.greenend.org.uk/~sgtatham/putty/"

function current_version()
   return exec_re("putty --version","(PuTTY: Release )(.*)(\n)",2)
end

