APP_NAME = "squid"
APP_URL = "http://www.squid-cache.org/"

function current_version()
   return exec_re("squid -v","(Squid Cache: Version )(.*)(\n)",2)
end

