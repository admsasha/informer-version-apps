APP_NAME = "Firefox"
APP_URL = "https://www.mozilla.org/firefox/"

function current_version()
   return exec_re("firefox --version","(Mozilla Firefox )(.*)(\n)",2)
end

