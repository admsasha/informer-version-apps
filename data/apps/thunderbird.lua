APP_NAME = "thunderbird"
APP_URL = "https://www.thunderbird.net"

function current_version()
   return exec_re("thunderbird --version","(Thunderbird )(.*)(\n)",2)
end

