APP_NAME = "Stellarium"
APP_URL = "http://www.stellarium.org"

function current_version()
   return exec_re("stellarium --version","(Stellarium )(.*)(\n)",2)
end

