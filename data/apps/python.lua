APP_NAME = "python"
APP_URL = "https://www.python.org"

function current_version()
   return exec_re("python --version","(Python )(.*)(\n)",2)
end

