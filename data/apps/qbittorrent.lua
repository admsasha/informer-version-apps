APP_NAME = "qBittorrent"
APP_URL = "https://www.qbittorrent.org/"

function current_version()
   return exec_re("qbittorrent --version","(qBittorrent v)(.*)(\n)",2)
end

