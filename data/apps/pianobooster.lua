APP_NAME = "PianoBooster"
APP_URL = "https://github.com/captnfab/PianoBooster"

function current_version()
   return exec_re("pianobooster --version","(pianobooster )(.*)(\n)",2)
end

