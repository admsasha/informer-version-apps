APP_NAME = "rust"
APP_URL = "https://www.rust-lang.org/"

function current_version()
   return exec_re("rustc --version","(rustc )(.*)(\n)",2)
end

