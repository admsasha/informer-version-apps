APP_NAME = "samba"
APP_URL = "https://www.samba.org/"

function current_version()
   return exec_re("smbcontrol --version","(Version )(\\d+\\.\\d+\\.\\d+)(.*)",2)
end

