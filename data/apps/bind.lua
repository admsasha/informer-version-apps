APP_NAME = "BIND"
APP_URL = "https://www.isc.org/bind/"

function current_version()
   return exec_re("named -V","(BIND )(.*)(-)",2)
end

