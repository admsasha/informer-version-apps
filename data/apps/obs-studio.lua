APP_NAME = "obs-studio"
APP_URL = "https://obsproject.com"

function current_version()
   return exec_re("obs --version","(OBS Studio - )(.*)( )",2)
end

