APP_NAME = "gnokii"
APP_URL = "https://www.gnokii.org/"

function current_version()
   return exec_re("gnokii --version","(GNOKII Version )(.*)(\n)",2)
end

