APP_NAME = "wine"
APP_URL = "https://www.winehq.org/"

function current_version()
   return exec_re("wine --version","(wine-)(.*)( )",2)
end

