APP_NAME = "Shotcut"
APP_URL = "https://www.shotcut.org/"

function current_version()
   return exec_re("shotcut --version","(Shotcut )(.*)(\n)",2)
end

