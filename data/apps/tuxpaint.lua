APP_NAME = "tuxpaint"
APP_URL = "http://www.tuxpaint.org"

function current_version()
   return exec_re("tuxpaint --version","( Version )(.*)( )",2)
end

