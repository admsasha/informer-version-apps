APP_NAME = "calibre"
APP_URL = "http://calibre-ebook.com/"

function current_version()
   return exec_re("calibre --version","(\\(calibre )(.*)(\\))",2)
end

