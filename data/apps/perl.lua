APP_NAME = "perl"
APP_URL = "https://www.perl.org"

function current_version()
   return exec_re("perl --version","(\\(v)(.*)(\\))",2)
end

