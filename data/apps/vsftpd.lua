APP_NAME = "vsftpd"
APP_URL = "https://security.appspot.com/vsftpd.html"

function current_version()
   return exec_re("bash -c \"vsftpd -v 0>&1\"","(vsftpd: version )(.*)(\n)",2)
end

