APP_NAME = "PostgreSQL"
APP_URL = "https://www.postgresql.org/"

function current_version()
   return exec_re("psql --version","(psql \\(PostgreSQL\\) )(.*)(\n)",2)
end

