APP_NAME = "remmina"
APP_URL = "https://remmina.org/"

function current_version()
   return exec_re("remmina --version","(Remmina - )(\\d+\\.\\d+\\.\\d+)(.*)",2)
end

