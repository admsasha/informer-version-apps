APP_NAME = "lmms"
APP_URL = "https://lmms.io/"

function current_version()
   return exec_re("lmms --version","(LMMS )(.*)(\n)",2)
end

