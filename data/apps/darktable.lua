APP_NAME = "darktable"
APP_URL = "https://www.darktable.org"

function current_version()
   return exec_re("darktable --version","(darktable-)(\\d\\.\\d\\.\\d)(.*)",2)
end

