APP_NAME = "tesseract"
APP_URL = "https://github.com/tesseract-ocr/"

function current_version()
   return exec_re("tesseract --version","(tesseract )(.*)(\n)",2)
end

