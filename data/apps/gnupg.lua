APP_NAME = "GnuPG"
APP_URL = "http://www.gnupg.org"

function current_version()
   return exec_re("gpg --version","(GnuPG\\) )(.*)(\n)",2)
end

