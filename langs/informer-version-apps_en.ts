<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>CTray</name>
    <message>
        <location filename="../CTray.cpp" line="4"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CTray.cpp" line="5"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormAbout</name>
    <message>
        <location filename="../FormAbout.ui" line="31"/>
        <source>Informer of new versions of programs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="131"/>
        <source>License: GPLv3+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="208"/>
        <source>Visit web site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.ui" line="226"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="15"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="18"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="19"/>
        <source>Date build:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAbout.cpp" line="20"/>
        <source>All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormAddApp</name>
    <message>
        <location filename="../FormAddApp.ui" line="20"/>
        <source>App:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAddApp.ui" line="43"/>
        <source>Append</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAddApp.ui" line="50"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormAddApp.cpp" line="18"/>
        <source>Adding an application for version control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormSetup</name>
    <message>
        <location filename="../FormSetup.ui" line="26"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.ui" line="39"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.ui" line="52"/>
        <source>Additional search paths:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.ui" line="68"/>
        <source>Version check frequency:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.ui" line="100"/>
        <source>Notify about new software versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.ui" line="113"/>
        <source>Update lua scripts for checker current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.cpp" line="13"/>
        <source>Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.cpp" line="20"/>
        <source>Not check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.cpp" line="21"/>
        <source>Once a day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.cpp" line="22"/>
        <source>Once a week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FormSetup.cpp" line="23"/>
        <source>Once a month</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="44"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="87"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="140"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="180"/>
        <source>Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="250"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="255"/>
        <source>Apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="260"/>
        <source>Current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Available version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="270"/>
        <source>Url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="301"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="347"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="37"/>
        <source>Informer of new versions of programs v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="96"/>
        <source>Controlled apps: 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="97"/>
        <source>Count of supported programs: 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="142"/>
        <source>Copy url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="238"/>
        <source>unknow error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="246"/>
        <location filename="../MainWindow.cpp" line="311"/>
        <source>Request current versions...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="259"/>
        <location filename="../MainWindow.cpp" line="328"/>
        <location filename="../MainWindow.cpp" line="366"/>
        <location filename="../MainWindow.cpp" line="484"/>
        <source>Request the latest versions of the software...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="339"/>
        <source>Search apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="339"/>
        <source>The entire current list will be deleted. Continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="347"/>
        <source>Search for installed programs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="402"/>
        <location filename="../MainWindow.cpp" line="471"/>
        <location filename="../MainWindow.cpp" line="510"/>
        <source>Informer Version Apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="402"/>
        <source>Version %1 update to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="448"/>
        <source>Controlled apps: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="468"/>
        <source>Adding app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="471"/>
        <source>Error adding %1. Perhaps the script %2.lua is broken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="507"/>
        <source>Count of supported programs: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="510"/>
        <source>Added support for %1 new programs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="29"/>
        <source>Informer Version Apps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WidgetWaiting</name>
    <message>
        <location filename="../WidgetWaiting.ui" line="58"/>
        <source>Waiting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
