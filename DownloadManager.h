#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QTimer>

class DownloadManager: public QObject{
    Q_OBJECT
    QNetworkAccessManager manager;

    public:
        DownloadManager();
        QByteArray execute(QString, bool async=true, QString method="get", QString data_post="", int timeout=10);
        int getStatusCode();
        QNetworkReply::NetworkError getErrorCode();
        QString getContentTypeHeader();
        bool isBusy();
        void setTimeout(int value);

    private:
        QEventLoop loop;
        QByteArray url_data;

        QNetworkReply::NetworkError errorCode;
        QVariant statusCode;
        QVariant httpReasonPhrase;
        QVariant httpContentTypeHeader;
        QTimer timer;

        QNetworkReply * reply;
        bool reply_busy;
        int _timeout;


    signals:
            void downloadOk(QUrl url, QByteArray data);
            void downloadError(QUrl url,QString errString);
            void downloadProgress(qint64,qint64);

    public slots:
        void downloadFinished(QNetworkReply *reply);
        void progress(qint64,qint64);
        void stop(bool force=false);

};


#endif // DOWNLOADMANAGER_H
