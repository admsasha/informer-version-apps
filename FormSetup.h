#ifndef FORMSETUP_H
#define FORMSETUP_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class FormSetup;
}

class FormSetup : public QDialog {
    Q_OBJECT

    public:
        explicit FormSetup(QWidget *parent = nullptr);
        ~FormSetup();

    private:
        Ui::FormSetup *ui;
        QSettings *confSettings;

    private slots:
        void apply();
};

#endif // FORMSETUP_H
