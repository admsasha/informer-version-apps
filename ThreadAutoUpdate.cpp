#include "ThreadAutoUpdate.h"

#include <QDir>
#include <QStandardPaths>

#include "DownloadManager.h"
#include "LuaScripts.h"

ThreadAutoUpdate::ThreadAutoUpdate(){
    QDir dirConfig(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    confSettings = new QSettings(dirConfig.absoluteFilePath("settings.ini"), QSettings::IniFormat);
    confSettings->setPath(QSettings::IniFormat, QSettings::UserScope, QDir::currentPath());
}

void ThreadAutoUpdate::run(){

    DownloadManager mng;

    QString currentStamp = confSettings->value("temp/lua_stamp","0").toString();

    while (1) {
        if (confSettings->value("setup/update_lua","1").toBool()==true){
            QString stamp =  mng.execute("https://appversions.dansoft.ru/api/CheckUpdateApps",false,"POST","check=z"+currentStamp);
            if (mng.getStatusCode()==200 and stamp!=""){
                if (currentStamp!=stamp){
                    int countUpdateFiles = updateLua();
                    confSettings->setValue("temp/lua_stamp",stamp);
                    emit update(countUpdateFiles);
                }
            }
        }

        QThread::sleep(86400);
    }
}


int ThreadAutoUpdate::updateLua(){
    int count=0;

    DownloadManager mng;
    QString listlua  = mng.execute("https://appversions.dansoft.ru/api/CheckUpdateApps",false,"POST","list=yes");
    if (mng.getStatusCode()!=200 or listlua.isEmpty()) return -1;

    QStringList listLuaScripts = LuaScripts::getScriptList();

    QStringList list = listlua.split("\n");
    for (QString line:list){
        if (line.isEmpty()) continue;
        QStringList pair = line.split(":");

        if (pair.size()!=2) continue;

        QString filename = pair.at(0);
        QString md5 = pair.at(1);
        QString tag = filename.left(filename.length()-4);

        if (listLuaScripts.contains(filename)){
            if (fileMd5(LuaScripts::getPathAppTag(tag))==md5) continue;
        }

        QByteArray data = mng.execute("https://appversions.dansoft.ru/apps/"+filename,false,"get");
        if (mng.getStatusCode()==200 and !data.isEmpty()){
            QFile file(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/apps/"+filename);
            if (file.open(QIODevice::WriteOnly | QIODevice::Text)){
                file.write(data);
                count++;
                file.close();
            }
        }

    }

    return count;
}

QByteArray ThreadAutoUpdate::fileMd5(const QString &fileName){
    QFile f(fileName);
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(QCryptographicHash::Md5);
        if (hash.addData(&f)) {
            return hash.result().toHex();
        }
    }
    return QByteArray();
}
