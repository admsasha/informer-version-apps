# README #
Informer of new versions of programs

Designed for those users who prefer to use the latest versions of the software. The resource is more focused on linux programs. But the versions of most programs are the same for all OS.


![informer-version-apps images](http://dansoft.krasnokamensk.ru/data/1029/informer-version-apps_en.png)


### How do I get set up? ###
qmake

make

make install


### Who do I talk to? ###
email: dik@inbox.ru

www: https://appversions.dansoft.ru/
