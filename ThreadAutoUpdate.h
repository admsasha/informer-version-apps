#ifndef THREADAUTOUPDATE_H
#define THREADAUTOUPDATE_H

#include <QObject>
#include <QThread>
#include <QSettings>
#include <QCryptographicHash>

class ThreadAutoUpdate : public QThread {
    Q_OBJECT
    public:
        ThreadAutoUpdate();
        void run();

    signals:
        void update(int countUpdateFiles);

    private:
        QSettings *confSettings;

        int updateLua();
        QByteArray fileMd5(const QString &fileName);
};

#endif // THREADAUTOUPDATE_H
