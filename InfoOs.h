#ifndef INFOOS_H
#define INFOOS_H

#include <QString>

struct INFORMATION_OS {
    QString id;
    QString typePkg;
};

class InfoOs {
    public:
        static INFORMATION_OS getInfoOs();
};

#endif // INFOOS_H
