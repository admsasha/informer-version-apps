#include <QApplication>
#include <QCommandLineParser>
#include <QTranslator>
#include <QLibraryInfo>

#include "MainWindow.h"
#include "config_informer.h"

int main(int argc, char *argv[]){
    QApplication app(argc, argv);
    QApplication::setQuitOnLastWindowClosed(false);

    QCoreApplication::setOrganizationName("DanSoft");
    QCoreApplication::setOrganizationDomain("dansoft.ru");
    QCoreApplication::setApplicationVersion(INFORMER_VERSION);
    QCoreApplication::setApplicationName("Informer Version Apps");

    QString locale = QLocale::system().name();

    QTranslator translator;
    translator.load(QString(GLOBAL_PATH_USERDATA)+QString("/langs/informer-version-apps_") + locale);
    app.installTranslator(&translator);

    QTranslator qtTranslator;
    qtTranslator.load("qt_"+locale,QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QCommandLineParser parser;
    parser.setApplicationDescription(QApplication::tr("Informer Version Apps"));
    parser.addVersionOption();
    parser.process(app);

    MainWindow form;
    form.show();

    return app.exec();
}
