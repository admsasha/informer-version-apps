#include "FormAddApp.h"
#include "ui_FormAddApp.h"

#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
#include <QtDebug>

#include "config_informer.h"
#include "LuaScripts.h"
#include "LuaContext.hpp"

FormAddApp::FormAddApp(QWidget *parent,QMap<QString,APP_STRUCT> &listSupportApps) :
    QDialog(parent),
    ui(new Ui::FormAddApp)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Adding an application for version control"));
    this->setWindowIcon(QIcon(QString(GLOBAL_PATH_USERDATA)+"/images/informer.png"));

    for (APP_STRUCT app:listSupportApps){
        ui->comboBox->addItem(app.name,app.tag);
    }    

    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(append()));

}

FormAddApp::~FormAddApp(){
    delete ui;
}

QString FormAddApp::getSelectTag(){
    return ui->comboBox->currentData().toString();
}

void FormAddApp::append(){
    accept();
}
