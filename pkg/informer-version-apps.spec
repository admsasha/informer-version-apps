Name:           informer-version-apps
Version:        1.0.0
Release:        1
Summary:        Informer Version Apps
Group:          Monitoring
License:        GPLv3+
Url:            https://bitbucket.org/admsasha/informer-version-apps
Source0:        https://bitbucket.org/admsasha/informer-version-apps/downloads/%{name}-%{version}.tar.gz

BuildRequires:  qt5-linguist-tools
BuildRequires:  boost-devel
BuildRequires:  pkgconfig(lua)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(Qt5Network)

%description
Informer Version Apps

%prep
%setup -q

%build
%qmake_qt5
%make

%install
%make_install INSTALL_ROOT=%{buildroot}

%files
%doc COPYING
%{_datadir}/%{name}
%{_bindir}/%{name}
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/applications/%{name}.desktop
